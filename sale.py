# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction
from trytond.modules.electrans_text_format.format import DescriptionFormatMixin


class Sale(DescriptionFormatMixin, metaclass=PoolMeta):
    __name__ = 'sale.sale'

    @classmethod
    def recompute_price_by_price_list(cls, sales, price_list):
        pool = Pool()
        SaleLine = pool.get('sale.line')
        to_save = []
        super().recompute_price_by_price_list(sales, price_list)
        for sale in sales:
            for line in sale.lines:
                if line.type != 'line' or line.parent:
                    continue
                description_to_set = line.get_description()
                if line.description != description_to_set:
                    line.description = description_to_set
                    to_save.append(line)
        if to_save:
            SaleLine.save(to_save)


class SaleLine(DescriptionFormatMixin, metaclass=PoolMeta):
    __name__ = 'sale.line'

    @fields.depends('product', 'description', '_parent_sale.party', '_parent_sale.lang', '_parent_sale.state')
    def on_change_product(self):
        super(SaleLine, self).on_change_product()
        new_description_lines = []
        current_description_lines = []
        # Saves both old and new descriptions splitted by lines
        if self.get_description():
            new_description_lines = self.get_description().split('\n')
        if self.description:
            current_description_lines = self.description.split('\n')
        # Saves each line from the new description which is not in the old one
        for line in new_description_lines:
            if line not in current_description_lines:
                current_description_lines.append(line)

        # Rebuilds the String with the new description lines to add
        # In case there's nothing new in the description, this has no effect
        self.description = '\n'.join(current_description_lines)

    def get_move(self, shipment_type):
        move = super(SaleLine, self).get_move(shipment_type)
        if move:
            move.description = move.get_description()
        return move

    def get_invoice_line(self):
        invoice_lines = super(SaleLine, self).get_invoice_line()
        if self.position:
            for line in invoice_lines:
                line.description = line.get_description()
        return invoice_lines


class ModifyHeader(metaclass=PoolMeta):
    __name__ = 'sale.modify_header'

    def transition_modify(self):
        with Transaction().set_context(keep_description=True):
            return super(ModifyHeader, self).transition_modify()
