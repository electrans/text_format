# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.modules.electrans_text_format.format import DescriptionFormatMixin


class StockMove(DescriptionFormatMixin, metaclass=PoolMeta):
    __name__ = 'stock.move'

    def on_change_product(self):
        super(StockMove, self).on_change_product()
        self.description = self.get_description()
