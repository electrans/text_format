# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import Pool
from trytond.transaction import Transaction
import logging
try:
    from jinja2 import Template as Jinja2Template
    jinja2_loaded = True
except ImportError:
    jinja2_loaded = False
    logging.getLogger('electrans_sale').error(
        'Unable to import jinja2. Install jinja2 package.')


class DescriptionFormat(ModelSQL, ModelView):
    "Description Format"
    __name__ = 'text.format'

    model = fields.Selection([
        ('sale.line', "Sale Line"),
        ('purchase.line', "Purchase Line"),
        ('stock.move', "Stock Move"),
        ('account.invoice.line', "Invoice Line"),
        ('purchase.request', "Purchase Request"),
        ('purchase.requisition.line', "Purchase Line Requisition"),
        ('purchase.request.quotation.line', "Purchase Request Quotation Line")
        ], "Move Origin")
    format_ = fields.Text(
        "Format",
        required=True)


class DescriptionFormatMixin(object):
    "Mixin to allow to format descriptions"

    @fields.depends('product', '_parent_product._context')
    def get_description(self):
        pool = Pool()
        Format = pool.get('text.format')
        SaleLine = pool.get('sale.line')
        PurchaseLine = pool.get('purchase.line')
        ShipmentOut = pool.get('stock.shipment.out')
        Product = pool.get('product.product')

        description = ''
        format_ = Format.search([('model', '=', self.__name__)], limit=1)
        if format_:
            if getattr(self, 'product', None):
                context = self.product._context
                if isinstance(self, SaleLine):
                    if self.sale and self.sale.party and self.sale.lang:
                        context['language'] = self.sale.lang.code
                        context['sale_customer'] = self.sale.party.id
                if getattr(self, 'origin', False) and self.origin.__name__ == 'sale.line':
                        context['language'] = self.origin.sale.lang.code
                        context['sale_customer'] = self.origin.sale.party.id
                if isinstance(self, PurchaseLine):
                    if self.purchase and self.purchase.party and self.purchase.party.lang:
                        context['language'] = self.purchase.party.lang.code
                if isinstance(getattr(self, 'shipment', None), ShipmentOut):
                    if self.shipment and self.shipment.customer and self.shipment.customer.lang:
                        context['language'] = self.shipment.customer.lang.code
                        context['sale_customer'] = self.shipment.customer.id
                with Transaction().set_context(context):
                    # re-browse the instance with the proper context to get translations if exits
                    if self.product:
                        self.product = Product(self.product.id)
            template = Jinja2Template(format_[0].format_)
            description = template.render({'record': self})
        return description
