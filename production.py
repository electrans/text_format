# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.

from trytond.pool import PoolMeta


class Production(metaclass=PoolMeta):
    __name__ = 'production'

    @classmethod
    def create_internal_shipments(
            cls, productions, to_location, split_by_planned_start_date):
        shipments = super(Production, cls).create_internal_shipments(
            productions, to_location, split_by_planned_start_date)
        for shipment in shipments:
            for move in shipment.moves:
                move.description = move.get_description()
                move.save()
        return shipments

    def _get_purchase_request(self):
        purchase_request = super(Production, self)._get_purchase_request()
        purchase_request.description = purchase_request.get_description()
        return purchase_request
