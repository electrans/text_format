# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import format
from . import sale
from . import invoice
from . import production
from . import purchase
from . import stock


def register():
    Pool.register(
        format.DescriptionFormat,
        sale.Sale,
        sale.SaleLine,
        invoice.InvoiceLine,
        production.Production,
        purchase.PurchaseLine,
        purchase.PurchaseRequest,
        purchase.QuotationLine,
        purchase.PurchaseRequisitionLine,
        stock.StockMove,
        module='electrans_text_format', type_='model')

    Pool.register(
        purchase.ModifyHeader,
        sale.ModifyHeader,
        module='electrans_text_format', type_='wizard')
