# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import fields
from trytond.modules.electrans_text_format.format import DescriptionFormatMixin
from trytond.transaction import Transaction


class PurchaseLine(DescriptionFormatMixin, metaclass=PoolMeta):
    __name__ = 'purchase.line'

    @fields.depends('note', 'product', 'description')
    def on_change_product(self):
        super(PurchaseLine, self).on_change_product()
        self.update_price_from_last_purchase()
        # if there is no product selected and it's getting executed from ModifyHeader wizard,
        # do not modify the description
        if not(not self.product and Transaction().context.get('keep_description', False)):
            self.description = self.get_description()

    def get_move(self, move_type):
        move = super(PurchaseLine, self).get_move(move_type)
        if move:
            move.description = move.get_description()
        return move

    def get_invoice_line(self):
        invoice_lines = super(PurchaseLine, self).get_invoice_line()
        for line in invoice_lines:
            line.description = line.get_description()
        return invoice_lines

    @classmethod
    def update_purchase_line(cls, request, line):
        super(PurchaseLine, cls).update_purchase_line(request, line)
        line.description = request.description if request.description \
            else line.get_description()


class PurchaseRequisitionLine(DescriptionFormatMixin, metaclass=PoolMeta):
    __name__ = 'purchase.requisition.line'

    @fields.depends('product')
    def on_change_product(self):
        super(PurchaseRequisitionLine, self).on_change_product()
        if self.product:
            self.description = self.get_description()


class PurchaseRequest(DescriptionFormatMixin, metaclass=PoolMeta):
    __name__ = 'purchase.request'


class ModifyHeader(metaclass=PoolMeta):
    __name__ = 'purchase.modify_header'

    def transition_modify(self):
        with Transaction().set_context(keep_description=True):
            return super(ModifyHeader, self).transition_modify()


class QuotationLine(DescriptionFormatMixin, metaclass=PoolMeta):
    __name__ = 'purchase.request.quotation.line'

    @fields.depends('product_to_request', 'description', 'request',
                    '_parent_request.description')
    def on_change_product_to_request(self):
        if self.product_to_request:
            self.description = self.get_description()
        else:
            self.description = self.request.description if self.request else ''

    @fields.depends('request', methods=['on_change_product_to_request'])
    def on_change_request(self):
        super(QuotationLine, self).on_change_request()
        self.on_change_product_to_request()
